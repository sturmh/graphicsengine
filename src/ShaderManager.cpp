//
//  ShaderManager.cpp
//  RenderEngine
//
//  Created by Eugene Sturm on 2/1/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "ShaderManager.h"
using namespace graphics;
using namespace std;

ShaderManager::ShaderManager(string shaderDirectory) {
  _shaderDir = shaderDirectory;
  
}

void ShaderManager::CreateProgram(string fpathVert, string fpathFrag) {
  GLint status;
  GLuint vertexShader;
  GLuint fragmentShader;
  GLuint shaderProgram;
  GLenum err = glGetError();
  
  string vertShaderContents = LoadFile(fpathVert);
  if(vertShaderContents == "") {
    return;
  }
  
  string fragShaderContents = LoadFile(fpathFrag);
  if(fragShaderContents == "") {
    return;
  }
  

  //create and compile the vertex shader
  const char* vc = vertShaderContents.c_str();
  vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vc, NULL );
	glCompileShader(vertexShader);
  
  //check for errors
  glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);
  if(status != GL_TRUE) {
    char buffer[512];
    glGetShaderInfoLog(vertexShader, 512, NULL, buffer);
    cout << buffer;
    return;
  }
  err = glGetError();
 
	// Create and compile the fragment shader
  const char* fc = fragShaderContents.c_str();
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fc, NULL);
	glCompileShader(fragmentShader);
  
  //check for errors
  glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &status);
  if(status != GL_TRUE) {
    char buffer[512];
    glGetShaderInfoLog(fragmentShader, 512, NULL, buffer);
    cout << buffer;
    return;
  }

	// Link the vertex and fragment shader into a shader program
  shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glBindFragDataLocation(shaderProgram, 0, "outColor");
	glLinkProgram(shaderProgram);
  
  glGetProgramiv (shaderProgram, GL_LINK_STATUS, &status);
  if (status == GL_FALSE)
  {
    GLint infoLogLength;
    glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &infoLogLength);
    
    GLchar *strInfoLog = new GLchar[infoLogLength + 1];
    glGetProgramInfoLog(shaderProgram, infoLogLength, NULL, strInfoLog);
    cout << "Linker failure: " << strInfoLog << "\n";
    delete[] strInfoLog;
  }
  
  err = glGetError();
  
  
  GLuint vao;
	//glGenVertexArrays( 1, &vao );
  err = glGetError();
	//glBindVertexArray( vao );
  err = glGetError();
  
  GLuint vbo;
	glGenBuffers( 1, &vbo );
  
	float vertices[] = {
		-0.5f,  0.5f, 1.0f, 0.0f, 0.0f, // Top-left
    0.5f,  0.5f, 0.0f, 1.0f, 0.0f, // Top-right
    0.5f, -0.5f, 0.0f, 0.0f, 1.0f, // Bottom-right
		-0.5f, -0.5f, 1.0f, 1.0f, 1.0f  // Bottom-left
	};
  
  //glBindBuffer( GL_ARRAY_BUFFER, vbo );
	//glBufferData( GL_ARRAY_BUFFER, sizeof( vertices ), vertices, GL_STATIC_DRAW);
  
  GLint posAttrib = glGetAttribLocation( shaderProgram, "position" );
  err = glGetError();
	glEnableVertexAttribArray( posAttrib );
  err = glGetError();
	glVertexAttribPointer( posAttrib, 2, GL_FLOAT, GL_FALSE, 5 * sizeof( float ), 0 );
  err = glGetError();
  
  

  
	GLint colAttrib = glGetAttribLocation( shaderProgram, "color" );
	glEnableVertexAttribArray( colAttrib );
	glVertexAttribPointer( colAttrib, 3, GL_FLOAT, GL_FALSE, 5 * sizeof( float ), (void*)( 2 * sizeof( float ) ) );

  
}

string ShaderManager::LoadFile(string fpath) {
  string content = "";
  ifstream fin;
  
  fin.open(fpath);
  if(fin.fail()) {
    return content;
  }
  fin.seekg(0, ios::end);
  content.resize(fin.tellg());
  fin.seekg(0, ios::beg);
  fin.read(&content[0], content.size());
  fin.close();
  
  return content;
}

void ShaderManager::LoadShaders() {
   
}