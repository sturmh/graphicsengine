//
//  SpriteRenderer.h
//  RenderEngine
//
//  Created by Eugene Sturm on 2/1/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __RenderEngine__SpriteRenderer__
#define __RenderEngine__SpriteRenderer__

#include "ShaderManager.h"
#include "ResourceManager.h"
#include "RenderQueue.h"
#include "RenderInstance.h"
#include "RenderGroup.h"
#include "Sprite.h"
#include <list>
#include <map>
#include <string>
#include <utility>


namespace graphics {
  
  class Sprite;
  
  
  typedef std::map<std::string, Sprite*> SpriteMap;
  typedef std::map<std::string, std::pair<RenderInstance*, Sprite*>> InstanceMap;
    
  class SpriteRenderer {
  public:
    SpriteRenderer(ShaderManager* shaderManager, ResourceManager* resourceManager);
    ~SpriteRenderer();
 
    void AddSprite(std::string name, Sprite* sprite);
    void RemoveSprite(std::string name);
    //void RemoveSprite(Sprite* sprite);
        
    /**
      *  Call this everyframe. Creates render instances for newly added sprites and
      *  submits RenderGroup to RenderQueue
    */
    void SubmitAll(RenderQueue* queue);
  private:
    ShaderManager* _shaderManager;
    ResourceManager* _resourceManager;
    
    InstanceMap _instanceMap;
    SpriteMap _newSprites;
    RenderGroup* _group;
  };
}

#endif /* defined(__RenderEngine__SpriteRenderer__) */
