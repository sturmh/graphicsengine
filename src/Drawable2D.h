//
//  Drawable2D.h
//  Graphics
//
//  Created by Eugene Sturm on 2/3/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __Graphics__Drawable2D__
#define __Graphics__Drawable2D__

#include <glm/glm.hpp>

namespace graphics {
    class Drawable2D {
    public:
        Drawable2D();
        
        
    
    protected:
      glm::mat4 _worldMatrix;
      glm::mat4 _scaleMatrix;
      glm::vec4 _color;
    };
}

#endif /* defined(__Graphics__Drawable2D__) */
