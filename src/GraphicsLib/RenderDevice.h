//
//  RenderDevice.h
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/26/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __GraphicsLib__RenderDevice__
#define __GraphicsLib__RenderDevice__

#include "OGLUtil.h"
#include "RenderQueue.h"

namespace graphics {
  
  /**
   * Encapsulates the rendering queue and any additional functionality
   * May not actually need this and could just use the RenderQueue
   */
  class RenderDevice {
  public:
    RenderDevice();
    ~RenderDevice();
    
    /**
     *  Clear backbuffer and execute all commands in RenderQueue
     */
    void Render();
    
  protected:
    /**
     *  Clear back and depth buffer
     */
    void Clear(float r = 0.f, float g = 0.f, float b = 0.f, float a = 1.f);
    
  public:
        RenderQueue* const queue;
    };
}

#endif /* defined(__GraphicsLib__RenderDevice__) */
