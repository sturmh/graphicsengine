//
//  BindVertexBuffer.cpp
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/28/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "BindVertexBuffer.h"
using namespace graphics;

BindVertexBuffer::BindVertexBuffer(GLint vbo)
    : RenderState(BIND_VERTEX_BUFFER), _vbo(vbo) {
    
}

RenderStateType BindVertexBuffer::GetType() {
    return _stateType;
}

void BindVertexBuffer::Execute() {
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
}