//
//  SetShaderVector.h
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/30/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __GraphicsLib__SetShaderVector__
#define __GraphicsLib__SetShaderVector__

#include "RenderState.h"

namespace graphics {
    
    typedef void (*UniformFunctionFloat)(GLint, GLsizei, const GLfloat*);
    typedef void (*UniformFunctionInt)(GLint, GLsizei, const GLint*);
    typedef void (*UniformFunctionUInt)(GLint, GLsizei, const GLuint*);
    
    enum VectorType {
        VECTOR_1,
        VECTOR_2,
        VECTOR_3,
        VECTOR_4    
    };
    
    class SetShaderVector : public RenderState {
    public:
        SetShaderVector(GLint location, GLsizei count, const GLfloat* value, VectorType vecType);
        SetShaderVector(GLint location, GLsizei count, const GLint* value, VectorType vecType);
        SetShaderVector(GLint location, GLsizei count, const GLuint* value, VectorType vecType);
        
        void Execute();
        
    private:
        void GetFunctionPointer();
        
    private:
        GLint _location;
        GLsizei _count;
        VarType _type;
        VectorType _vecType;
        
        union {
            UniformFunctionFloat _functionFloat;
            UniformFunctionInt _functionInt;
            UniformFunctionUInt _functionUint;
        };
        
        union {
            const GLfloat* _vf;
            const GLuint*  _vui;
            const GLint* _vi;
        };
    };
}

#endif /* defined(__GraphicsLib__SetShaderVector__) */
