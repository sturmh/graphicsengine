//
//  SetShaderVariable.cpp
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/30/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "SetShaderVariable.h"
using namespace graphics;

SetShaderVariable::SetShaderVariable(GLint location, GLfloat v0)
  : RenderState(SET_UNIFORM) {
    _fv[0] = v0;
    _location = location;
    _valCount = 1;
    _type = FLOAT;
}

SetShaderVariable::SetShaderVariable(GLint location, GLfloat v0, GLfloat v1)
: RenderState(SET_UNIFORM) {
  _fv[0] = v0;
  _fv[1] = v1;
  _location = location;
  _valCount = 2;
  _type = FLOAT;
}

SetShaderVariable::SetShaderVariable(GLint location, GLfloat v0, GLfloat v1, GLfloat v2)
: RenderState(SET_UNIFORM) {
  _fv[0] = v0;
  _fv[1] = v1;
  _fv[2] = v2;
  _location = location;
  _valCount = 3;
  _type = FLOAT;
}

SetShaderVariable::SetShaderVariable(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3)
: RenderState(SET_UNIFORM) {
  _fv[0] = v0;
  _fv[1] = v1;
  _fv[2] = v2;
  _fv[3] = v3;
  _location = location;
  _valCount = 4;
  _type = FLOAT;
}

SetShaderVariable::SetShaderVariable(GLint location, GLint v0)
: RenderState(SET_UNIFORM) {
  _iv[0] = v0;
  _location = location;
  _valCount = 1;
  _type = INT;
}

SetShaderVariable::SetShaderVariable(GLint location, GLint v0, GLint v1)
: RenderState(SET_UNIFORM) {
  _iv[0] = v0;
  _iv[1] = v1;
  _location = location;
  _valCount = 2;
  _type = INT;
}

SetShaderVariable::SetShaderVariable(GLint location, GLint v0, GLint v1, GLint v2)
: RenderState(SET_UNIFORM) {
  _iv[0] = v0;
  _iv[1] = v1;
  _iv[2] = v2;
  _location = location;
  _valCount = 3;
  _type = INT;
}

SetShaderVariable::SetShaderVariable(GLint location, GLint v0, GLint v1, GLint v2, GLint v3)
: RenderState(SET_UNIFORM) {
  _iv[0] = v0;
  _iv[1] = v1;
  _iv[2] = v2;
  _iv[3] = v3;
  _location = location;
  _valCount = 4;
  _type = INT;
}


SetShaderVariable::SetShaderVariable(GLint location, GLuint v0)
: RenderState(SET_UNIFORM) {
  _uiv[0] = v0;
  _location = location;
  _valCount = 1;
  _type = UINT;
}

SetShaderVariable::SetShaderVariable(GLint location, GLuint v0, GLuint v1)
: RenderState(SET_UNIFORM) {
  _uiv[0] = v0;
  _uiv[1] = v1;
  _location = location;
  _valCount = 2;
  _type = UINT;
}

SetShaderVariable::SetShaderVariable(GLint location, GLuint v0, GLuint v1, GLuint v2)
: RenderState(SET_UNIFORM) {
  _uiv[0] = v0;
  _uiv[1] = v1;
  _uiv[2] = v2;
  _location = location;
  _valCount = 3;
  _type = UINT;
}

SetShaderVariable::SetShaderVariable(GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3)
: RenderState(SET_UNIFORM) {
  _uiv[0] = v0;
  _uiv[1] = v1;
  _uiv[2] = v2;
  _uiv[3] = v3;
  _location = location;
  _valCount = 4;
  _type = UINT;
}


void SetShaderVariable::Execute() {
  switch(_type) {
    case FLOAT:
      switch(_valCount) {
        case 1:
          glUniform1f(_location, _fv[0]);
          break;
        case 2:
          glUniform2f(_location, _fv[0], _fv[1]);
          break;
        case 3:
          glUniform3f(_location, _fv[0], _fv[1], _fv[2]);
          break;
        case 4:
          glUniform4f(_location, _fv[0], _fv[1], _fv[2], _fv[3]);
          break;
        default:
          break;
      }
      break;
    case UINT:
      switch(_valCount) {
        case 1:
          glUniform1i(_location, _iv[0]);
          break;
        case 2:
          glUniform2i(_location, _iv[0], _iv[1]);
          break;
        case 3:
          glUniform3i(_location, _iv[0], _iv[1], _iv[2]);
          break;
        case 4:
          glUniform4i(_location, _iv[0], _iv[1], _iv[2], _iv[3]);
          break;
        default:
          break;
      }
      break;
    case INT:
      switch(_valCount) {
        case 1:
          glUniform1ui(_location, _uiv[0]);
          break;
        case 2:
          glUniform2ui(_location, _uiv[0], _uiv[1]);
          break;
        case 3:
          glUniform3ui(_location, _uiv[0], _uiv[1], _uiv[2]);
          break;
        case 4:
          glUniform4ui(_location, _uiv[0], _uiv[1], _uiv[2], _uiv[3]);
          break;
        default:
          break;
      }
      break;
    default:
      break;
  }
}
