//
//  RenderState.cpp
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/28/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "RenderState.h"
using namespace graphics;


RenderState::RenderState(RenderStateType type)
: _stateType(type) {
    
}

RenderStateType RenderState::GetType() {
  return _stateType;
}