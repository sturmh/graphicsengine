//
//  UseShaderProgram.cpp
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/28/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "UseShaderProgram.h"
using namespace graphics;

UseShaderProgram::UseShaderProgram(GLuint shaderProgram)
: RenderState(USE_SHADER_PROGRAM), _program(shaderProgram) {
    
}

RenderStateType UseShaderProgram::GetType() {
    return _stateType;
}

void UseShaderProgram::Execute() {
    glUseProgram(_program);
}