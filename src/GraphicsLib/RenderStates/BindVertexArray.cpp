//
//  BindVertexArray.cpp
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/30/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "BindVertexArray.h"
using namespace graphics;

BindVertexArray::BindVertexArray(GLuint vao)
: RenderState(BIND_VERTEX_ARRAY_OBJECT), _vao(vao) {
  
}

void BindVertexArray::Execute() {
  glBindVertexArray(_vao);
}