//
//  BindElementBuffer.cpp
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/28/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "BindElementBuffer.h"
using namespace graphics;

BindElementBuffer::BindElementBuffer(GLint ebo)
  : RenderState(BIND_ELEMENT_BUFFER), _ebo(ebo) {
    
}

void BindElementBuffer::Execute() {
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
}