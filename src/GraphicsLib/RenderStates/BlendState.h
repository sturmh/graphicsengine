//
//  BlendState.h
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/31/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __GraphicsLib__BlendState__
#define __GraphicsLib__BlendState__

#include "RenderState.h"

namespace graphics {
    class BlendState : public RenderState {
    public:
        BlendState(bool isEnable);
        
        void SetBlendEquation(GLenum modeRGB, GLenum modeAlpha);
        void SetBlendFunc(GLenum srcRGB, GLenum dstRGB, GLenum srcAlpha, GLenum dstAlpha);
        void SetBlendColor(GLclampf r, GLclampf g, GLclampf b, GLclampf a);
        
        void Execute();
    private:
        bool _isEnabled;
        bool _hasBlendEquation;
        bool _hasBlendFunc;
        bool _hasBlendColor;
        
        GLenum _modeRGB;
        GLenum _modeAlpha;
        
        GLenum _srcRGB;
        GLenum _dstRGB;
        GLenum _srcAlpha;
        GLenum _dstAlpha;
        
        GLclampf _blendColor[4];        
    };
}

#endif /* defined(__GraphicsLib__BlendState__) */
