//
//  UseShaderProgram.h
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/28/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __GraphicsLib__UseShaderProgram__
#define __GraphicsLib__UseShaderProgram__

#include "RenderState.h"

namespace graphics {
    class UseShaderProgram : public RenderState {
    public:
        UseShaderProgram(GLuint shaderProgram);
        
        RenderStateType GetType();
        void Execute();
    private:
        GLuint _program;
    };
}

#endif /* defined(__GraphicsLib__UseShaderProgram__) */
