//
//  SetShaderVector.cpp
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/30/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "SetShaderVector.h"
using namespace graphics;

SetShaderVector::SetShaderVector(GLint location, GLsizei count, const GLfloat* value, VectorType vecType)
  : RenderState(SET_UNIFORM) {
    _count = count;
    _vf = value;
    _type = FLOAT;
    _vecType = vecType;
    GetFunctionPointer();
  
}
SetShaderVector::SetShaderVector(GLint location, GLsizei count, const GLint* value, VectorType vecType)
  : RenderState(SET_UNIFORM) {
    _count = count;
    _vi = value;
    _type = INT;
    _vecType = vecType;
    GetFunctionPointer();
}
SetShaderVector::SetShaderVector(GLint location, GLsizei count, const GLuint* value, VectorType vecType)
  : RenderState(SET_UNIFORM) {
    _count = count;
    _vui = value;
    _type = UINT;
    _vecType = vecType;
    GetFunctionPointer();
}

void SetShaderVector::Execute() {
  if(_type == UINT) {
    _functionUint(_location, _count, _vui);
  } else if(_type == INT) {
    _functionInt(_location, _count, _vi);
  } else if(_type == FLOAT) {
    _functionFloat(_location, _count, _vf);
  } else {
  }
}

void SetShaderVector::GetFunctionPointer() {
  if(_type == UINT) {
    switch(_vecType) {
      case VECTOR_1:
        _functionUint = glUniform1uiv;
        break;
      case VECTOR_2:
        _functionUint = glUniform2uiv;
        break;
      case VECTOR_3:
        _functionUint = glUniform3uiv;
        break;
      case VECTOR_4:
        _functionUint = glUniform4uiv;
        break;
      default:
        break;
    }
  } else if(_type == INT) {
    switch(_vecType) {
      case VECTOR_1:
        _functionInt = glUniform1iv;
        break;
      case VECTOR_2:
        _functionInt = glUniform2iv;
        break;
      case VECTOR_3:
        _functionInt = glUniform3iv;
        break;
      case VECTOR_4:
        _functionInt = glUniform4iv;
        break;
      default:
        break;
    }    
  } else if(_type == FLOAT) {
    switch(_vecType) {
      case VECTOR_1:
        _functionFloat = glUniform1fv;
        break;
      case VECTOR_2:
        _functionFloat = glUniform2fv;
        break;
      case VECTOR_3:
        _functionFloat = glUniform3fv;
        break;
      case VECTOR_4:
        _functionFloat = glUniform4fv;
        break;
      default:
        break;
    }
  } else {
  }
}