//
//  SetShaderVariable.h
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/30/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __GraphicsLib__SetShaderVariable__
#define __GraphicsLib__SetShaderVariable__

#include "RenderState.h"

namespace graphics {
        
    class SetShaderVariable : public RenderState {
    public:
        SetShaderVariable(GLint location, GLfloat v0);
        SetShaderVariable(GLint location, GLfloat v0, GLfloat v1);
        SetShaderVariable(GLint location, GLfloat v0, GLfloat v1, GLfloat v2);
        SetShaderVariable(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);
        
        SetShaderVariable(GLint location, GLint v0);
        SetShaderVariable(GLint location, GLint v0, GLint v1);
        SetShaderVariable(GLint location, GLint v0, GLint v1, GLint v2);
        SetShaderVariable(GLint location, GLint v0, GLint v1, GLint v2, GLint v3);

        SetShaderVariable(GLint location, GLuint v0);
        SetShaderVariable(GLint location, GLuint v0, GLuint v1);
        SetShaderVariable(GLint location, GLuint v0, GLuint v1, GLuint v2);
        SetShaderVariable(GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3);
        
        void Execute();
    private:
        GLint _location;
        int _valCount;
        VarType _type;
        
        union {
            GLint   _iv[4];
            GLuint  _uiv[4];
            GLfloat _fv[4];
        };
        
        
    };
}

#endif /* defined(__GraphicsLib__SetShaderVariable__) */
