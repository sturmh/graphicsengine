//
//  BindElementBuffer.h
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/28/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __GraphicsLib__BindElementBuffer__
#define __GraphicsLib__BindElementBuffer__

#include "RenderState.h"

namespace graphics {
    class BindElementBuffer : public RenderState {
    public:
        BindElementBuffer(GLint ebo);
        
        void Execute();
        RenderStateType GetType();
    private:
        GLint _ebo;
    };
}
#endif /* defined(__GraphicsLib__BindElementBuffer__) */
