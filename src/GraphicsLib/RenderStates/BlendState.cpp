//
//  BlendState.cpp
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/31/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "BlendState.h"
using namespace graphics;

BlendState::BlendState(bool isEnabled)
  : RenderState(BLEND_STATE), _isEnabled(isEnabled), _hasBlendEquation(false), _hasBlendFunc(false), _hasBlendColor(false) {
  
}

void BlendState::SetBlendColor(GLclampf r, GLclampf g, GLclampf b, GLclampf a) {
  _hasBlendColor = true;
  _blendColor[0] = r;
  _blendColor[1] = g;
  _blendColor[2] = b;
  _blendColor[3] = a;
}

void BlendState::SetBlendEquation(GLenum modeRGB, GLenum modeAlpha) {
  _hasBlendEquation = true;
  _modeAlpha        = modeAlpha;
  _modeRGB          = modeRGB;
}

void BlendState::SetBlendFunc(GLenum srcRGB, GLenum dstRGB, GLenum srcAlpha, GLenum dstAlpha) {
  _hasBlendFunc = true;
  _srcAlpha     = srcAlpha;
  _srcRGB       = srcRGB;
  _dstAlpha     = dstAlpha;
  _dstRGB       = dstRGB;
}

void BlendState::Execute() {
  if(_isEnabled) {
    glEnable(GL_BLEND);

    if(_hasBlendColor) {
      glBlendColor(_blendColor[0], _blendColor[1], _blendColor[2], _blendColor[3]);
    }
    if(_hasBlendEquation) {
      glBlendEquationSeparate(_modeRGB, _modeAlpha);
    }
    if(_hasBlendFunc) {
      glBlendFuncSeparate(_srcRGB, _dstRGB, _srcAlpha, _dstAlpha); 
    }
  } else {
    glDisable(GL_BLEND);
  }
}