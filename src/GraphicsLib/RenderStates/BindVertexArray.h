//
//  BindVertexArray.h
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/30/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __GraphicsLib__BindVertexArray__
#define __GraphicsLib__BindVertexArray__

#include "RenderState.h"

namespace graphics {
    class BindVertexArray : public RenderState {
    public:
        BindVertexArray(GLuint vao);
        void Execute();
    private:
        GLuint _vao;
    };
}

#endif /* defined(__GraphicsLib__BindVertexArray__) */
