//
//  BindVertexBuffer.h
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/28/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __GraphicsLib__BindVertexBuffer__
#define __GraphicsLib__BindVertexBuffer__

#include "RenderState.h"

namespace graphics {
    class BindVertexBuffer : public RenderState {
    public:
        BindVertexBuffer(GLint vbo);
        
        void Execute();
        RenderStateType GetType();
    private:
        GLint _vbo;
    };
}

#endif /* defined(__GraphicsLib__BindVertexBuffer__) */
