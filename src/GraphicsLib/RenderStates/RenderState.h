//
//  RenderState.h
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/28/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __GraphicsLib__RenderState__
#define __GraphicsLib__RenderState__

#include "RenderCommand.h"

namespace graphics {
  
  enum VarType {
    FLOAT,
    UINT,
    INT
  };
  
  enum RenderStateType {
    BIND_ELEMENT_BUFFER,
    BIND_VERTEX_BUFFER,
    BIND_VERTEX_ARRAY_OBJECT,
    USE_SHADER_PROGRAM,
    SET_UNIFORM,
    BLEND_STATE
  };
  
  class RenderState : public RenderCommand {
  public:
      RenderState(RenderStateType type);
      
      RenderStateType GetType();
  protected:
      RenderStateType _stateType;
  };
}

#endif /* defined(__GraphicsLib__RenderState__) */
