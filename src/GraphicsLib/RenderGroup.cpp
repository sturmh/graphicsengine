//
//  RenderGroup.cpp
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/26/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "RenderGroup.h"
using namespace graphics;

RenderGroup::RenderGroup() {
  _stateGroup = new StateGroup();
}

RenderGroup::~RenderGroup() {
  delete _stateGroup;
  _stateGroup = 0;
}

void RenderGroup::Add(RenderState* state) {
  _stateGroup->Add(state);
}

void RenderGroup::Add(RenderInstance* renderInstance) {
  _renderInstances.push_back(renderInstance);
}

void RenderGroup::Clear() {
  _stateGroup->Clear();
  _renderInstances.clear();
}

StateList RenderGroup::GetGroupStates() {
  return _stateGroup->GetStateList();
}

RenderInstanceList RenderGroup::GetRenderInstances() {
  return _renderInstances;
}