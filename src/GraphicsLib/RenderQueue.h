//
//  RenderQueue.h
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/28/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __GraphicsLib__RenderQueue__
#define __GraphicsLib__RenderQueue__

#include "RenderGroup.h"

#include <list>

namespace graphics {
  
  typedef std::list<RenderCommand*> CommandList;
  
  /**
   * Converts all submitted RenderInstances/RenderGroups into a
   * list of RenderCommands to be executed in order.
   *
   * May want to do some sorting of state changes in future
   */
  class RenderQueue {
  public:
    friend class RenderDevice;
  public:
    /**
     * Deconstruct a RenderGroup into a set of RenderCommands and add them
     * to the commandList
     */
    void Submit(RenderGroup* group);
      
    /**
     * Deconstruct a RenderInstnace into a set of RenderCommands and add them
     * to the commandList
     */
    void Submit(RenderInstance* instance);
    
    /**
     * Unimplemented
     */
    void Submit(DrawCall* drawCall, StateGroup* stateGroups, int stateGroupCount);
      
  protected:
      RenderQueue();
      
      /**
       * Execute all RenderCommands in the commandList
       * Note: May need to insure proper order of commands (ex. UseProgram before setting uniforms)
       */
      void ExecuteCommands();
  private:
      CommandList _commandList;
  };
}

#endif /* defined(__GraphicsLib__RenderQueue__) */
