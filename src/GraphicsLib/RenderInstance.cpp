//
//  RenderInstance.cpp
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/26/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "RenderInstance.h"
using namespace graphics;

RenderInstance::RenderInstance()
    : _drawCall(0) {
}

void RenderInstance::AddStateGroup(StateGroup* states) {
  _stateGroupList.push_back(states);
}

void RenderInstance::SetDrawCall(DrawCall* drawCall) {
    _drawCall = drawCall;
}

StateGroupList RenderInstance::GetStateGroupList() {
  return _stateGroupList;
}

StateList RenderInstance::GetStateList() {
  StateList states;
  for(StateGroupList::iterator it = _stateGroupList.begin(); it != _stateGroupList.end(); ++it) {
    StateList subStates = (*it)->GetStateList();
    states.insert(states.begin(), subStates.begin(), subStates.end());
  }
  return states;
}

DrawCall* RenderInstance::GetDrawCall() {
  return _drawCall;
}
