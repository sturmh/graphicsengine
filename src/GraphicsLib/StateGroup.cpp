//
//  StateGroup.cpp
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/26/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "StateGroup.h"
using namespace graphics;

StateGroup::StateGroup() {
}

void StateGroup::Add(RenderState* state) {
    _stateList.push_back(state);
}

void StateGroup::Clear() {
    _stateList.clear();
}

StateList StateGroup::GetStateList() {
  return _stateList;
}