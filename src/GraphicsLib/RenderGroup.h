//
//  RenderGroup.h
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/26/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __GraphicsLib__RenderGroup__
#define __GraphicsLib__RenderGroup__

#include <list>
#include "RenderInstance.h"
#include "StateGroup.h"

namespace graphics {
  
  typedef std::list<RenderInstance*> RenderInstanceList;
  
  /**
   *  Set of RenderInstances, each with their own set of states, and a common set o
   *  shared states
   */
  class RenderGroup {
  public:
    RenderGroup();
    ~RenderGroup();
    
    /**
     *  Add state to shared StateGroup
     */
    void Add(RenderState* state);
    
    /**
     * Add instancce to group
     */
    void Add(RenderInstance* instance);
      
    void Clear();

    StateList GetGroupStates();
    RenderInstanceList GetRenderInstances();
  private:
    StateGroup*         _stateGroup;
    RenderInstanceList  _renderInstances;
  };
}

#endif /* defined(__GraphicsLib__RenderGroup__) */
