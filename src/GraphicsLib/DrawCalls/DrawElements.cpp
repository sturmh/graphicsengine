//
//  DrawElements.cpp
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/28/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "DrawElements.h"
using namespace graphics;

DrawElements::DrawElements()
  : DrawCall(DRAW_ELEMENTS) {
  
}

void DrawElements::Execute() {
    glDrawElements(drawMode, elementCount, indicesType, indicesBuffer);
}

