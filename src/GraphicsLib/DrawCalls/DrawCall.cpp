//
//  DrawCall.cpp
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/28/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "DrawCall.h"
using namespace graphics;

DrawCall::DrawCall(DrawCallType type)
  : _drawCallType(type) {
  
}

DrawCallType DrawCall::GetType() {
  return _drawCallType;
}