//
//  DrawCall.h
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/28/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __GraphicsLib__DrawCall__
#define __GraphicsLib__DrawCall__

#include "RenderCommand.h"

namespace graphics {
  enum DrawCallType {
    DRAW_ELEMENTS,
    DRAW_ARRAYS
  };
  
  class DrawCall : public RenderCommand {
  public:
    DrawCall(DrawCallType type);
    
    virtual void Execute() = 0;
    DrawCallType GetType();
  protected:
    DrawCallType _drawCallType;
  };
}

#endif /* defined(__GraphicsLib__DrawCall__) */
