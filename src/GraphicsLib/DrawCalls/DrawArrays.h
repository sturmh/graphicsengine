//
//  DrawArrays.h
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/28/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __GraphicsLib__DrawArrays__
#define __GraphicsLib__DrawArrays__

#include "DrawCall.h"

namespace graphics {
  class DrawArrays : public DrawCall {
  public:
    DrawArrays();
    void Execute();
    
  public:
    GLenum drawMode;
    GLint startIndex;
    GLint indexCount;
        
  };
}

#endif /* defined(__GraphicsLib__DrawArrays__) */
