//
//  DrawArrays.cpp
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/28/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "DrawArrays.h"
using namespace graphics;

DrawArrays::DrawArrays()
  : DrawCall(DRAW_ARRAYS) {
  
}

void DrawArrays::Execute() {
    glDrawArrays(drawMode, startIndex, indexCount);
}
