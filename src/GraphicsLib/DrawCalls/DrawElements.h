//
//  DrawElements.h
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/28/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __GraphicsLib__DrawElements__
#define __GraphicsLib__DrawElements__

#include "Drawcall.h"

namespace graphics {
  class DrawElements : public DrawCall {
  public:
    DrawElements();
    
    void Execute();
    
  public:
    GLenum drawMode;
    GLsizei elementCount;
    GLenum indicesType;
    GLvoid* indicesBuffer; //or is it offset?!?
        
    };
}

#endif /* defined(__GraphicsLib__DrawElements__) */
