//
//  RenderDevice.cpp
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/26/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "RenderDevice.h"
using namespace graphics;

RenderDevice::RenderDevice()
  : queue(new RenderQueue()) {
    
}

RenderDevice::~RenderDevice() {
  delete queue;
}

void RenderDevice::Render() {
  Clear();
  queue->ExecuteCommands();
}

void RenderDevice::Clear(float r, float g, float b, float a) {
  glClearColor(r, g, b, a);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}

