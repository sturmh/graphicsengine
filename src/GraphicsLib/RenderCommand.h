//
//  RenderCommand.h
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/28/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __GraphicsLib__RenderCommand__
#define __GraphicsLib__RenderCommand__

#include "OGLUtil.h"

namespace graphics {
  class RenderCommand {
  public:
    virtual ~RenderCommand();
    virtual void Execute() = 0;
  private:
  };
}

#endif /* defined(__GraphicsLib__RenderCommand__) */
