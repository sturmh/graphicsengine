//
//  RenderInstance.h
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/26/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __GraphicsLib__RenderInstance__
#define __GraphicsLib__RenderInstance__

#include "StateGroup.h"
#include "DrawCalls/DrawCall.h"
#include <list>

namespace graphics {
  
  typedef std::list<StateGroup*> StateGroupList;
  
  class RenderInstance {
  public:
    RenderInstance();
    
    void SetDrawCall(DrawCall* drawCall);
    void AddStateGroup(StateGroup* states);
    StateGroupList GetStateGroupList();
    StateList GetStateList();
    DrawCall* GetDrawCall();
  private:
      StateGroupList  _stateGroupList;
      DrawCall*       _drawCall;
  };
}

#endif /* defined(__GraphicsLib__RenderInstance__) */
