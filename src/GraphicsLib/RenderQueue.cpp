//
//  RenderQueue.cpp
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/28/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "RenderQueue.h"
using namespace graphics;

RenderQueue::RenderQueue() {
    
}

void RenderQueue::Submit(RenderGroup* group) {
  StateList groupStates = group->GetGroupStates();
  
  //push groupstates onto commandlist
  _commandList.insert(_commandList.end(), groupStates.begin(), groupStates.end());
  
  //extract each instances states and draw call
  RenderInstanceList instances = group->GetRenderInstances();
  for(RenderInstanceList::iterator it = instances.begin(); it != instances.end(); ++it) {
    Submit((*it));
  }
}

void RenderQueue::Submit(RenderInstance* renderInstance) {
  StateList instanceStates = renderInstance->GetStateList();
  _commandList.insert(_commandList.end(), instanceStates.begin(), instanceStates.end());
  _commandList.push_back(renderInstance->GetDrawCall());
}

void RenderQueue::Submit(DrawCall* drawCall, StateGroup* stateGroups, int stateGroupCount) {
    
}

void RenderQueue::ExecuteCommands() {
  for(CommandList::iterator it = _commandList.begin(); it != _commandList.end(); ++it) {
    (*it)->Execute();
  }
}