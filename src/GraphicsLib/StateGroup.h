//
//  StateGroup.h
//  GraphicsLib
//
//  Created by Eugene Sturm on 1/26/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __GraphicsLib__StateGroup__
#define __GraphicsLib__StateGroup__

#include <list>
#include "RenderStates/RenderState.h"

namespace graphics {
  
  typedef std::list<RenderState*> StateList;
  
  class StateGroup {
  public:
    StateGroup();
      
    void Add(RenderState* state);
    void Clear();
    StateList GetStateList();
  private:
    StateList _stateList;
        
  };
}

#endif /* defined(__GraphicsLib__StateGroup__) */
