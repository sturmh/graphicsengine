
#include "Graphics.h"
#include <gl/glfw.h>
#include "OGLUtil.h"

int main(int argc, char* argv[]) {
 
  int height      = 600;  int width       = 600;
  int redBits     = 16;   int greenBits   = 16;  int blueBits    = 16;  int alphaBits   = 16;
  int depthBits   = 8;    int stencilBits = 0;
  int mode        = GLFW_WINDOW;

  if(!glfwInit()) {
    return 1;
  }
  
  int major = 0;  int minor = 0;  int rev = 0;
  glfwGetVersion(&major, &minor, &rev );
  glfwSwapInterval(0);
  
  
  glfwOpenWindowHint( GLFW_OPENGL_VERSION_MAJOR, 3 );
  glfwOpenWindowHint( GLFW_OPENGL_VERSION_MINOR, 2 );
  glfwOpenWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );
  glfwOpenWindowHint( GLFW_WINDOW_NO_RESIZE, GL_TRUE );
  
  if(!glfwOpenWindow( width, height, redBits, greenBits, blueBits, alphaBits, depthBits, stencilBits, mode )) {
    fprintf( stderr, "Failed to open GLFW window\n" );
    glfwTerminate();
    return -1;
    
  }
  
  

  
  
  /*
  graphics::GraphicsEngine* engine = new graphics::GraphicsEngine();
  Sprite* s;
  s->SetPosition(glm::vec2(1.f, 2.f));
  s->SetVisible(true);
  s->SetDimension(1, 2);
  
  engine->AddSprite("boom", s);
  
  s->Move(1, 1);
  engine->Render();
  */
  
  return 1;
}

/*
#include <iostream>
#include "GraphicsEngine.h"
#include "GraphicsLib.h"
#include <gl/glfw.h>
#include <glm/glm.hpp>
#include <stdio.h>
#include <string>
#include <iostream>
#include "GraphicsEngine.h"
#include "GraphicsLib.h"

/// Shader sources
const char* vertexSource =
"#version 150\n"
"in vec2 position;"
"in vec3 color;"
"out vec3 Color;"
"void main() {"
"	Color = color;"
"	gl_Position = vec4( position, 0.0, 1.0 );"
"}";
const char* fragmentSource =
"#version 150\n"
"uniform vec3 triangleColor;"
"in vec3 Color;"
"out vec4 outColor;"
"void main() {"
"	outColor = vec4( triangleColor,  1.f);"

"}";


void GLFWCALL keyCallback( int key, int action ) {
    printf("%d, %d\n", key, action);
}
void GLFWCALL charCallback( int character, int action );



int main(int argc, const char * argv[])
{
  
    
    graphics::GraphicsEngine g;
    int height      = 600;
    int width       = 600;
    int redBits     = 16;
    int greenBits   = 16;
    int blueBits    = 16;
    int alphaBits   = 16;
    int depthBits   = 8;
    int stencilBits = 0;
    int mode        = GLFW_WINDOW;
    GLenum err = 0;
    if(!glfwInit()) {
        return 1;
    }
    
    int major = 0;
    int minor = 0;
    int rev = 0;
    glfwGetVersion(&major, &minor, &rev );
    glfwSwapInterval(0);
    
    
    glfwOpenWindowHint( GLFW_OPENGL_VERSION_MAJOR, 3 );
    glfwOpenWindowHint( GLFW_OPENGL_VERSION_MINOR, 2 );
    glfwOpenWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );
    glfwOpenWindowHint( GLFW_WINDOW_NO_RESIZE, GL_TRUE );
    
    if(!glfwOpenWindow( width, height, redBits, greenBits, blueBits, alphaBits, depthBits, stencilBits, mode )) {
        fprintf( stderr, "Failed to open GLFW window\n" );
        glfwTerminate();
        return -1;
        
    }
    
    glfwSetWindowTitle( "OpenGL" );
    glfwSetKeyCallback( keyCallback );
    
    err = glGetError();
    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        return -1;
    }
    err = glGetError();
    // Create Vertex Array Object
	GLuint vao;
	glGenVertexArrays( 1, &vao );
	glBindVertexArray( vao );
    
	// Create a Vertex Buffer Object and copy the vertex data to it
	GLuint vbo;
	glGenBuffers( 1, &vbo );
    
	float vertices[] = {
		-0.5f,  0.5f, 1.0f, 0.0f, 0.0f, // Top-left
        0.5f,  0.5f, 0.0f, 1.0f, 0.0f, // Top-right
        0.5f, -0.5f, 0.0f, 0.0f, 1.0f, // Bottom-right
		-0.5f, -0.5f, 1.0f, 1.0f, 1.0f  // Bottom-left
	};
    
    graphics::BindVertexBuffer bvb(vbo);
	glBindBuffer( GL_ARRAY_BUFFER, vbo );
    
	glBufferData( GL_ARRAY_BUFFER, sizeof( vertices ), vertices, GL_STATIC_DRAW);
    
	// Create an element array
	GLuint ebo;
	glGenBuffers( 1, &ebo );
    
	GLuint elements[] = {
		0, 1, 2,
		2, 3, 0
	};
    
    
    
    graphics::BindElementBuffer beb(ebo);
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, ebo );
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( elements ), elements, GL_STATIC_DRAW );
    
	// Create and compile the vertex shader
	GLuint vertexShader = glCreateShader( GL_VERTEX_SHADER );
	glShaderSource( vertexShader, 1, &vertexSource, NULL );
	glCompileShader( vertexShader );
    
    err = glGetError();
    
	// Create and compile the fragment shader
	GLuint fragmentShader = glCreateShader( GL_FRAGMENT_SHADER );
	glShaderSource( fragmentShader, 1, &fragmentSource, NULL );
	glCompileShader( fragmentShader );
    
	// Link the vertex and fragment shader into a shader program
	GLuint shaderProgram = glCreateProgram();
	glAttachShader( shaderProgram, vertexShader );
	glAttachShader( shaderProgram, fragmentShader );
	glBindFragDataLocation( shaderProgram, 0, "outColor" );
	glLinkProgram( shaderProgram );
    
    
    
    graphics::UseShaderProgram usp(shaderProgram);
    //	glUseProgram( shaderProgram );
    
	// Specify the layout of the vertex data
	GLint posAttrib = glGetAttribLocation( shaderProgram, "position" );
	glEnableVertexAttribArray( posAttrib );
	glVertexAttribPointer( posAttrib, 2, GL_FLOAT, GL_FALSE, 5 * sizeof( float ), 0 );
    
	GLint colAttrib = glGetAttribLocation( shaderProgram, "color" );
	glEnableVertexAttribArray( colAttrib );
	glVertexAttribPointer( colAttrib, 3, GL_FLOAT, GL_FALSE, 5 * sizeof( float ), (void*)( 2 * sizeof( float ) ) );
    
 
     //IMPORTANT: Have to call useProgram before messing with uniforms
     
    
    glUseProgram(shaderProgram);
    float s[3] = {0.f, 1.f, 1.f};
    GLint loc = glGetUniformLocation(shaderProgram, "triangleColor");
    graphics::SetShaderVariable sv(loc, s[0], s[1], s[2]);
    
    
    
    graphics::DrawElements de;
    de.drawMode = GL_TRIANGLES;
    de.elementCount = 6;
    de.indicesType = GL_UNSIGNED_INT;
    de.indicesBuffer = 0;
    
    graphics::StateGroup sg;
    sg.Add(&beb);
    sg.Add(&bvb);
    sg.Add(&usp);
    sg.Add(&sv);
    
    graphics::RenderInstance ri;
    ri.SetDrawCall(&de);
    ri.AddStateGroup(&sg);
    
    graphics::RenderDevice device;
    device.queue->Submit(&ri);
    
    //reset timer
    glfwGetTime();
    double currentTime = 0;
    double lastFrame = 0;
    double frameTime = 0;
    int frameCount = 0;
    double fpsTimeCount = 0;
    
    while( glfwGetWindowParam( GLFW_OPENED ) )
    {
        
        if ( glfwGetKey( GLFW_KEY_ESC ) == GLFW_PRESS )
            break;
        
        device.Render();
        glfwSwapBuffers();
        
        
        currentTime = glfwGetTime();
        frameTime = currentTime - lastFrame;
        fpsTimeCount += frameTime;
        lastFrame = currentTime;
        frameCount++;
        
        if(fpsTimeCount > 1.0) {
            std::cout << frameCount << "\n";
            frameCount = 0;
            fpsTimeCount = 0;
        }
        
        
    }
    
    
    
    
    
    glfwCloseWindow();
    glfwTerminate();
    return 0;
}*/

