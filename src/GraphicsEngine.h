//
//  GraphicsEngine.h
//  RenderEngine
//
//  Created by Eugene Sturm on 2/1/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __RenderEngine__GraphicsEngine__
#define __RenderEngine__GraphicsEngine__

#include "RenderDevice.h"
#include "ShaderManager.h"
#include "ResourceManager.h"
#include "SpriteRenderer.h"

namespace graphics {
    class GraphicsEngine {
    public:
        GraphicsEngine();
        ~GraphicsEngine();
        
        void AddDrawable(Sprite* sprite);
        
        void Render();
    private:
        RenderDevice* _device;
        ResourceManager* _resourceManager;
        ShaderManager* _shaderManager;
        SpriteRenderer* _spriteRenderer;
        
    };
}

#endif /* defined(__RenderEngine__GraphicsEngine__) */
