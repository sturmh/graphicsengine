//
//  SpriteRenderer.cpp
//  RenderEngine
//
//  Created by Eugene Sturm on 2/1/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "SpriteRenderer.h"
using namespace graphics;

SpriteRenderer::SpriteRenderer(ShaderManager* shaderManager, ResourceManager* resourceManager) {
  _resourceManager = resourceManager;
  _shaderManager = shaderManager;
  _group = new RenderGroup();
}

SpriteRenderer::~SpriteRenderer() {
  delete _group;
}

void SpriteRenderer::AddSprite(std::string name, Sprite* sprite) {
  _newSprites[name] = sprite;
}

void SpriteRenderer::RemoveSprite(std::string name) {

}

void SpriteRenderer::SubmitAll(RenderQueue* queue) {
  //submit all current sprites to RenderGroup. Update dirty sprites
  //check for new sprites, create RenderInstances for them, submit to RenderGroup and save

  _group->Clear();
  
  //group->Add(_shaderManager->GetProgram("spriteProgram")->getStateGroup());
  //group->Add(_resourceManager->GetGeometry("square")->GetStateGroup());
  //add view and projection matrix

  for(InstanceMap::iterator it = _instanceMap.begin(); it != _instanceMap.end(); ++it) {

    Sprite* sprite = it->second.second;
    if(sprite->IsVisible() == false) {
     continue; 
    }
    
    RenderInstance* instance = it->second.first;
    _group->Add(instance);
  }
  
  if(_newSprites.empty() == false) {
    for(SpriteMap::iterator it = _newSprites.begin(); it != _newSprites.end(); ++it) {
      Sprite* sprite = it->second;
      RenderInstance* instance = new RenderInstance();
      
      //do stuff
      
      _group->Add(instance);
      _instanceMap[it->first] = std::make_pair(instance, sprite);
      _newSprites[it->first] = 0;
    }
  }
  
  queue->Submit(_group);
}