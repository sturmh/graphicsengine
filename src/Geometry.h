//
//  Geometry.h
//  Graphics
//
//  Created by Eugene Sturm on 2/3/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __Graphics__Geometry__
#define __Graphics__Geometry__

namespace graphics {
    class Geometry {
    public:
        VertexBuffer vb;
        IndexBuffer ib;
    };
}

#endif /* defined(__Graphics__Geometry__) */
