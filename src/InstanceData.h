//
//  InstanceData.h
//  Graphics
//
//  Created by Eugene Sturm on 2/3/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef Graphics_InstanceData_h
#define Graphics_InstanceData_h

#include <glm/glm.hpp>

namespace graphics {
  struct InstanceData {
    glm::mat4 worldMatrix;
  };
}

#endif
