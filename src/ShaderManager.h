//
//  ShaderManager.h
//  RenderEngine
//
//  Created by Eugene Sturm on 2/1/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __RenderEngine__ShaderManager__
#define __RenderEngine__ShaderManager__

#include <string>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <string.h>
#include "OGLUtil.h"
#include <sstream>
#include "ShaderProgram.h"
using namespace std;

namespace graphics {
    class ShaderManager {
    public:
        ShaderManager(string shaderDirectory);
        
        void CreateProgram(string fpathVert, string fpathFrag);
      ShaderProgram* GetProgram(string name);
    private:
        string LoadFile(string fpath);
        void LoadShaders();
        void Parse(string source);
    private:
        string _shaderDir;
    };
}

#endif /* defined(__RenderEngine__ShaderManager__) */
