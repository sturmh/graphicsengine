//
//  Sprite.cpp
//  Graphics
//
//  Created by Eugene Sturm on 2/2/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "Sprite.h"
using namespace graphics;
using namespace std;

Sprite::Sprite() {
  _isVisible = true;
}

void Sprite::SetVisible(bool isVisible) {
  _isVisible = isVisible;
}

bool Sprite::IsVisible() {
    return _isVisible;
}

Sprite::~Sprite() {
  
}
