//
//  FrameData.h
//  Graphics
//
//  Created by Eugene Sturm on 2/3/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef Graphics_FrameData_h
#define Graphics_FrameData_h

#include <glm/glm.hpp>

namespace graphics {
  struct FrameData {
    glm::mat4 viewMatrix;
    glm::mat4 projMatrix;
  };
}

#endif
