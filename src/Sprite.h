//
//  Sprite.h
//  Graphics
//
//  Created by Eugene Sturm on 2/2/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __Graphics__Sprite__
#define __Graphics__Sprite__

#include "SpriteRenderer.h"
#include "Drawable2D.h"
#include <string>
#include <glm/glm.hpp>
using namespace std;

namespace graphics {
  class SpriteRenderer;
  
  class Sprite : public Drawable2D{
    friend SpriteRenderer;
    
    public:
      Sprite();
      ~Sprite();
    
      void SetVisible(bool isVisible);
      bool IsVisible();

    private:
      bool _isVisible;
      
    };
}

#endif /* defined(__Graphics__Sprite__) */
