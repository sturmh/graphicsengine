//
//  GraphicsEngine.cpp
//  RenderEngine
//
//  Created by Eugene Sturm on 2/1/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "GraphicsEngine.h"
using namespace graphics;

GraphicsEngine::GraphicsEngine() {
  glewExperimental = GL_TRUE;
  if (glewInit() != GLEW_OK) {
    printf("Failed to initialize GLEW\n");
    return;
  }
  
  printf("Vendor:   %s\n", glGetString(GL_VENDOR));
  printf("Renderer: %s\n", glGetString(GL_RENDERER));
  printf("Version:  %s\n", glGetString(GL_VERSION));
  printf("GLSL:     v%s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
  
   
  
  
  
  _device = new RenderDevice();
  _resourceManager = new ResourceManager();
  _shaderManager = new ShaderManager("~/Development");
  _shaderManager->CreateProgram("/Users/sturm/Projects/Graphics/media/temp.vert", "/Users/sturm/Projects/Graphics/media/temp.frag");
  _spriteRenderer = new SpriteRenderer(_shaderManager,_resourceManager);
  
}

GraphicsEngine::~GraphicsEngine() {
  delete _spriteRenderer;
  delete _shaderManager;
  delete _resourceManager;
  delete _device;
}

void GraphicsEngine::Render() {
  //setup perframe data
  
  //have each subrender submit drawables
  _spriteRenderer->SubmitAll(_device->queue);
  
  //execute renderqueue commands
  _device->Render();
  
}