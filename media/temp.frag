#version 150

uniform vec3 triangleColor;
in vec3 Color;

out vec4 outColor;

void main() {
  outColor = vec4(triangleColor, 1.f);
}